var express = require("express");
var path = require('path');
var app = express();
var request = require('request');

app.set('port', process.env.PORT || 3000);
app.use("/", express.static(path.join(__dirname, 'public')));

app.get('/',function(req,res) {
    res.sendfile('views/index.html');
})

app.get('/callout',function(req,res) {

    var url = req.query.url;

    request.get({url:url},function (error, response, body) {
        res.json(body);
    });
})
app.get('/control',function(req,res) {
    res.sendfile('views/index.html');
})

app.get('/splash',function(req,res) {
    res.sendfile('views/release/index.html');
})

app.listen(app.get('port'));